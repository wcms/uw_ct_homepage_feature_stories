<?php

/**
 * @file
 * uw_ct_homepage_feature_stories.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_homepage_feature_stories_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function uw_ct_homepage_feature_stories_node_info() {
  $items = array(
    'homepage_feature_stories' => array(
      'name' => t('Feature Stories'),
      'base' => 'node_content',
      'description' => t('Feature stories share information that is relevant and timely in promoting Waterloo.'),
      'has_title' => '1',
      'title_label' => t('Headline'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
