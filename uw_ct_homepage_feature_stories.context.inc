<?php

/**
 * @file
 * uw_ct_homepage_feature_stories.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_homepage_feature_stories_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'homepage_feature_stories_context';
  $context->description = 'Displays the quicktabs for the feature stories.';
  $context->tag = 'homepage';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'quicktabs-homepage_feature_stories' => array(
          'module' => 'quicktabs',
          'delta' => 'homepage_feature_stories',
          'region' => 'banner',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Displays the quicktabs for the feature stories.');
  t('homepage');
  $export['homepage_feature_stories_context'] = $context;

  return $export;
}
