<?php

/**
 * @file
 * uw_ct_homepage_feature_stories.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function uw_ct_homepage_feature_stories_taxonomy_default_vocabularies() {
  return array(
    'homepage_feature_stories_audiences' => array(
      'name' => 'Feature Stories Audiences',
      'machine_name' => 'homepage_feature_stories_audiences',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
