<?php

/**
 * @file
 * uw_ct_homepage_feature_stories.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_homepage_feature_stories_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create homepage_feature_stories content'.
  $permissions['create homepage_feature_stories content'] = array(
    'name' => 'create homepage_feature_stories content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any homepage_feature_stories content'.
  $permissions['delete any homepage_feature_stories content'] = array(
    'name' => 'delete any homepage_feature_stories content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own homepage_feature_stories content'.
  $permissions['delete own homepage_feature_stories content'] = array(
    'name' => 'delete own homepage_feature_stories content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in homepage_feature_stories_audiences'.
  $permissions['delete terms in homepage_feature_stories_audiences'] = array(
    'name' => 'delete terms in homepage_feature_stories_audiences',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any homepage_feature_stories content'.
  $permissions['edit any homepage_feature_stories content'] = array(
    'name' => 'edit any homepage_feature_stories content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own homepage_feature_stories content'.
  $permissions['edit own homepage_feature_stories content'] = array(
    'name' => 'edit own homepage_feature_stories content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in homepage_feature_stories_audiences'.
  $permissions['edit terms in homepage_feature_stories_audiences'] = array(
    'name' => 'edit terms in homepage_feature_stories_audiences',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'enter homepage_feature_stories revision log entry'.
  $permissions['enter homepage_feature_stories revision log entry'] = array(
    'name' => 'enter homepage_feature_stories revision log entry',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_feature_stories authored by option'.
  $permissions['override homepage_feature_stories authored by option'] = array(
    'name' => 'override homepage_feature_stories authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_feature_stories authored on option'.
  $permissions['override homepage_feature_stories authored on option'] = array(
    'name' => 'override homepage_feature_stories authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_feature_stories promote to front page option'.
  $permissions['override homepage_feature_stories promote to front page option'] = array(
    'name' => 'override homepage_feature_stories promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_feature_stories published option'.
  $permissions['override homepage_feature_stories published option'] = array(
    'name' => 'override homepage_feature_stories published option',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_feature_stories revision option'.
  $permissions['override homepage_feature_stories revision option'] = array(
    'name' => 'override homepage_feature_stories revision option',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_feature_stories sticky option'.
  $permissions['override homepage_feature_stories sticky option'] = array(
    'name' => 'override homepage_feature_stories sticky option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  return $permissions;
}
