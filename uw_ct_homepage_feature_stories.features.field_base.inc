<?php

/**
 * @file
 * uw_ct_homepage_feature_stories.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function uw_ct_homepage_feature_stories_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_faculty_author'.
  $field_bases['field_faculty_author'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_faculty_author',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'org_default_bg' => 'University of Waterloo',
        'org_ahs_bg' => 'Health',
        'org_art_bg' => 'Arts',
        'org_eng_bg' => 'Engineering',
        'org_env_bg' => 'Environment',
        'org_mat_bg' => 'Mathematics',
        'org_sci_bg' => 'Science',
        'org_cgc_bg' => 'Conrad Grebel',
        'org_ren_bg' => 'Renison',
        'org_stj_bg' => 'St. Jerome\'s',
        'org_stp_bg' => 'St. Paul\'s',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_faculty_type_stories'.
  $field_bases['field_faculty_type_stories'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_faculty_type_stories',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'tid' => array(
        0 => 'tid',
      ),
    ),
    'locked' => 0,
    'module' => 'taxonomy',
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => 'homepage_feature_type',
          'parent' => 0,
        ),
      ),
      'options_list_callback' => 'title_taxonomy_allowed_values',
    ),
    'translatable' => 0,
    'type' => 'taxonomy_term_reference',
  );

  return $field_bases;
}
